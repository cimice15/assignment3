1. First clone this repo (https://gitlab.com/cimice15/assignment3)<br/>
2. In MySqlWorkbench execute script: "script_import.sql" to create user "usr" and empty scheme "organization".
3. Open project in ecplipse, add libraries in "lib" folder to project and run tests in any order because they are indipendent.<br/><br/>
Note Well! To make the project as close as possible to real use, in "hibernate.cfg.xml" I've selected to "update" database in any session. So
if you want to re-execute tests you must drop all tables in "organization" database.  