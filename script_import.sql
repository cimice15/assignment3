CREATE USER IF NOT EXISTS 'usr'@'localhost' IDENTIFIED BY 'usr';

GRANT ALL PRIVILEGES ON * . * TO 'usr'@'localhost';


ALTER USER 'usr'@'localhost' IDENTIFIED WITH mysql_native_password BY 'usr';

CREATE SCHEMA IF NOT EXISTS `organization` DEFAULT CHARACTER SET utf8 ;

USE `organization` ;
