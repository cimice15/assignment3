package com.organization.entities;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="Report")
public class Report {

		@Id
		@Column(name="ID",length=8)
		private String id;
		
		@Temporal(TemporalType.DATE)
		Date publishDate;
		
		@ManyToOne
		@JoinColumn(name="fiscalCode")
		private OfficeStaff officeStaff;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public Date getOpblishDate() {
			return publishDate;
		}

		public void setOpblishDate(Date publishDate) {
			this.publishDate = publishDate;
		}

		public OfficeStaff getOp() {
			return officeStaff;
		}

		public void setOp(OfficeStaff officeStaff) {
			this.officeStaff = officeStaff;
		}

		@Override
		public String toString() {
			SimpleDateFormat dr = new SimpleDateFormat("dd-MM-yyyy");
			return "Report [id=" + id + ", publishDate=" + publishDate + "]";
		}

		public Report(String id, Date pd, OfficeStaff officeStaff) {
			this.id = id;
			this.publishDate = pd;
			this.officeStaff = officeStaff;
		}

		public Report() {
			super();
		}

	
}
