package com.organization.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="software")
public class Software {

	@Override
	public String toString() {
		return "Software [id=" + id + ", softN=" + softN + ", softH=" + softH + ", teln=" + teln + "]";
	}

	@Id
	@Column(name="ID",length=10)
	private String id;
	
	public Software() {
		super();
	}

	@Column(name="softwareName",length=64)
	private String softN;
	
	@Column(name="softwareHouse",length=64)
	private String softH;
	
	@Column(name="Tel_number",length=20)
	private String teln;

	@OneToMany(mappedBy = "sw")
    private Set<Paycheck> payCheck = new HashSet<Paycheck>();
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSoftN() {
		return softN;
	}

	public void setSoftN(String softN) {
		this.softN = softN;
	}

	public String getSoftH() {
		return softH;
	}

	public void setSoftH(String softH) {
		this.softH = softH;
	}

	public String getTeln() {
		return teln;
	}

	public void setTeln(String teln) {
		this.teln = teln;
	}

	public Software(String id, String softN, String softH, String teln) {
		this.id = id;
		this.softN = softN;
		this.softH = softH;
		this.teln = teln;
	}	
	
	
}
