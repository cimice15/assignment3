package com.organization.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.organization.utility.StringsEnum;

@Entity
@Table(name="office_staff")
public class OfficeStaff {

	
	public Set<Report> getReport() {
		return report;
	}

	public void setReport(Set<Report> report) {
		this.report = report;
	}

	public Set<WorkingRelationship> getRap() {
		return workRel;
	}

	public void setRap(Set<WorkingRelationship> workRel) {
		this.workRel = workRel;
	}

	@Id
	@Column(name="fiscalCode",length=16)
	private String fc;
	
	@Column(name="Name", nullable=false,length=64)
	private String name;
	
	@Column(name="Surname", nullable=false,length=64)
	private String surname;
	
	@Column(name="Role", nullable=false,length=12)
	private String role;
	
	@Override
	public String toString() {
		return "OfficeStaff [fc=" + fc + ", name=" + name + ", surname=" + surname + ", role=" + role + "]";
	}
	
	@OneToMany(mappedBy = "officeStaff")
    private Set<Paycheck> payCheck = new HashSet<Paycheck>();

	@ManyToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="manager_id")
	private OfficeStaff manager;

	@OneToMany(mappedBy="manager",cascade={CascadeType.ALL})
	private Set<OfficeStaff> subordinates = new HashSet<OfficeStaff>();
	
	@OneToMany(mappedBy="officeStaff")
	private Set<Report> report;
	
	@OneToMany(mappedBy = "officeStaff")
    private Set<WorkingRelationship> workRel = new HashSet<WorkingRelationship>();
	

	public String getFc() {
		return fc;
	}

	public void setFc(String fc) {
		this.fc = fc;
	}

	public String getNome() {
		return name;
	}

	public void setNome(String name) {
		this.name = name;
	}

	public String getCogname() {
		return surname;
	}

	public void setCogname(String surname) {
		this.surname = surname;
	}

	public String getRuolo() {
		return role;
	}

	

	public OfficeStaff() {
		super();
	}

	public void setRuolo(String role) {
		this.role = StringsEnum.inputString(role).getText();
	}

	public OfficeStaff getManager() {
		return manager;
	}

	public void setManager(OfficeStaff manager) {
		this.manager = manager;
	}

	public Set<OfficeStaff> getSubordinates() {
		return subordinates;
	}

	

	public OfficeStaff(String fc, String name, String surname, String role,
			OfficeStaff manager, Set<OfficeStaff> subordinates) {
		super();
		this.fc = fc;
		this.name = name;
		this.surname = surname;
		this.role = StringsEnum.inputString(role).getText();
		this.manager = manager;
		this.subordinates = subordinates;
	}

	public void setSubordinates(Set<OfficeStaff> subordinates) {
		this.subordinates = subordinates;
	}

	
}
