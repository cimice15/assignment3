package com.organization.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="employee")
public class Employee {
	

	@Id
	@Column(name="FC",length=16)
	private String fc;
	
	@Column(name="Name", nullable=false,length=64)
	private String name;
	
	@Column(name="Surname", nullable=false,length=64)
	private String surname;

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(
		      name="work",
    	      joinColumns=@JoinColumn(name="FC"),
    	      inverseJoinColumns=@JoinColumn(name="VATnumber")
		      
		   )
    private Set<Company> company = new HashSet<>();
	
	public Set<Paycheck> getBp() {
		return payCheck;
	}

	public void setBp(Set<Paycheck> payCheck) {
		this.payCheck = payCheck;
	}

	@OneToMany(mappedBy = "emp")
    private Set<WorkingRelationship> workRel = new HashSet<WorkingRelationship>();
	
	@OneToMany(mappedBy = "emp")
    private Set<Paycheck> payCheck = new HashSet<Paycheck>();
	
//	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "employee")
//    private Set<OfficeStaff> officeStaff = new HashSet<>();

	public Set<WorkingRelationship> getRap() {
		return workRel;
	}

	public void setRap(Set<WorkingRelationship> workRel) {
		this.workRel = workRel;
	}

	public String getFc() {
		return fc;
	}

	public void setFc(String fc) {
		this.fc = fc;
	}

	public String getNome() {
		return name;
	}

	public void setNome(String name) {
		this.name = name;
	}

	public String getCogname() {
		return surname;
	}

	public void setCogname(String surname) {
		this.surname = surname;
	}

	public Set<Company> getCompany() {
		return company;
	}

	public void setCompany(Set<Company> company) {
		this.company = company;
	}

	public Employee(String fc, String name, String surname,Set<Company> company) {
		this.company = company;
		this.fc = fc;
		this.name = name;
		this.surname = surname;
	}

	public Employee() {
		super();
	}

	@Override
	public String toString() {
		return "Employee [fc=" + fc + ", name=" + name + ", surname=" + surname + "]";
	}
	
	
	
}
