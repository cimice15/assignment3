package com.organization.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="working_relationship")
public class WorkingRelationship {

	  	@Override
	public String toString() {
		return "WorkingRelationship [protNumber=" + protNumber + "]";
	}

		@ManyToOne
	    @JoinColumn(name = "FC")
	    private Employee emp;
	
	  	@ManyToOne
	    @JoinColumn(name = "fiscalCode")
	    private OfficeStaff officeStaff;
	
	    @Id
	    @Column(name = "protocolNumber", length=8)
	    private int protNumber;
	    
	    public Employee getEmp() {
			return emp;
		}

		public WorkingRelationship(Employee emp, OfficeStaff officeStaff, int protNumber) {
			this.emp = emp;
			this.officeStaff = officeStaff;
			this.protNumber = protNumber;
		}

		public void setEmp(Employee emp) {
			this.emp = emp;
		}

		public WorkingRelationship() {
			super();
		}

		public OfficeStaff getOp() {
			return officeStaff;
		}

		public void setOp(OfficeStaff officeStaff) {
			this.officeStaff = officeStaff;
		}

		public int getNumProt() {
			return protNumber;
		}

		public void setNumProt(int protNumber) {
			this.protNumber = protNumber;
		}

		
}
