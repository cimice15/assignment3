package com.organization.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="paycheck")
public class Paycheck {

	public Paycheck() {
		super();
	}

	@Id
    @Column(name = "valNum", length=12)
    private String valNum;
	 
	@Column(name = "period_competence",length=15)
    private String perComp;
	
	public Paycheck(String valNum, String perComp, Employee emp, OfficeStaff officeStaff, Software sw) {
		this.valNum = valNum;
		this.perComp = perComp;
		this.emp = emp;
		this.officeStaff = officeStaff;
		this.sw = sw;
	}

	public String getNumVid() {
		return valNum;
	}

	public void setNumVid(String valNum) {
		this.valNum = valNum;
	}

	public String getPerComp() {
		return perComp;
	}

	public void setPerComp(String perComp) {
		this.perComp = perComp;
	}

	public Employee getEmp() {
		return emp;
	}

	public void setEmp(Employee emp) {
		this.emp = emp;
	}

	public OfficeStaff getOp() {
		return officeStaff;
	}

	public void setOp(OfficeStaff officeStaff) {
		this.officeStaff = officeStaff;
	}

	public Software getSw() {
		return sw;
	}

	public void setSw(Software sw) {
		this.sw = sw;
	}

	@ManyToOne
    @JoinColumn(name = "FC")
    private Employee emp;

	@ManyToOne
    @JoinColumn(name = "fiscalCode")
    private OfficeStaff officeStaff;
	
	@ManyToOne
    @JoinColumn(name = "id")
    private Software sw;

   
	
}
