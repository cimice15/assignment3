package com.organization.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
@Table(name="company")
public class Company {

	@Id
	@Column(name="VATnumber",length=11)
	private String vatNumber;
	
	@Column(name="Name", nullable=false,length=64)
	private String name;

	@ManyToMany(mappedBy = "company")
	@Cascade(value = { CascadeType.ALL, CascadeType.SAVE_UPDATE })
    private Set<Employee> emp = new HashSet<Employee>();
	
	public Set<Employee> getEmp() {
		return emp;
	}

	public void setEmp(Set<Employee> emp) {
		this.emp = emp;
	}

	public Company() {
		super();
	}

	public Company(String vatNumber, String name) {
		this.vatNumber = vatNumber;
		this.name = name;
	}

	public String getVATnumber() {
		return vatNumber;
	}

	public void setVATnumber(String vatNumber) {
		this.vatNumber = vatNumber;
	}

	public String getNome() {
		return name;
	}

	public void setNome(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "company [vatNumber=" + vatNumber + ", name=" + name + "]";
	}
	
}
