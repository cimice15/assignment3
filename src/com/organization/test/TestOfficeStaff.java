package com.organization.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Iterator;
import java.util.Set;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.organization.entities.OfficeStaff;
import com.organization.operations.OfficeStaffCRUD;

//Sorts by method name
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestOfficeStaff {

//CREATE OPERATIONS
	@Test
	public void addOfficeStaff() {
		OfficeStaff pu1=OfficeStaffCRUD.addOfficeStaff("AGLM4QE6M23B018P","Mirko","Agarla","titolare");
		OfficeStaff pu2=OfficeStaffCRUD.addOfficeStaff("GRLMRK96M23B018G","Matteo","Conti","coordinatore");
		//check if values on DB by using read operation 
		OfficeStaff ck1=OfficeStaffCRUD.searchOfficeStaffByFC(pu1.getFc());
		assertEquals(pu1.toString(),ck1.toString());
		OfficeStaff ck2=OfficeStaffCRUD.searchOfficeStaffByFC(pu2.getFc());
		assertEquals(pu2.toString(),ck2.toString());
		assertNotNull(pu1);
		assertNotNull(pu2);
	}
	
	@Test
	public void addOfficeStaffManagerId() {
		OfficeStaff pu1=OfficeStaffCRUD.addOfficeStaffManagerId("AGLM4QE6M23B018P","Mirko","Agarla","titolare","GRLMRK96M23B018G");
		//check if values on DB by using read operation 
		OfficeStaff ck1=OfficeStaffCRUD.searchOfficeStaffByFC(pu1.getFc());
		assertEquals(pu1.toString(),ck1.toString());
		assertNotNull(pu1);
	}

//READ OPERATIONS

	@Test
	public void readOfficeStaff(){
		Set<OfficeStaff> officeStaff= OfficeStaffCRUD.readOfficeStaff();
		assertNotNull(officeStaff);
	}
	
//UPDATE OPERATIONS
	@Test
	public void updateOfficeStaff(){
		OfficeStaff mg= OfficeStaffCRUD.updateOfficeStaff("AGLM4QE6M23B018P","impiegato");
		OfficeStaff ck1=OfficeStaffCRUD.searchOfficeStaffByFC(mg.getFc());
		assertEquals(ck1.getRuolo(),mg.getRuolo());
	}


//DELETE OPERATIONS
	@Test
	public void zdeleteOfficeStaff(){
		OfficeStaff officeStaff= OfficeStaffCRUD.zdeleteOfficeStaffByFc("GRLMRK96M23B018G");
		//check deleted OffiseStaff
		assertEquals("GRLMRK96M23B018G",officeStaff.getFc());
	}	

//SEARCH OPERATIONS
	@Test
	public void searchOfficeStaffByFC(){
		OfficeStaff mg=OfficeStaffCRUD.searchOfficeStaffByFC("AGLM4QE6M23B018P");
		assertEquals("AGLM4QE6M23B018P",mg.getFc());
	}

	@Test
	public void searchOfficeStaffByManagerId(){
		Set<OfficeStaff> mg=OfficeStaffCRUD.searchOfficeStaffByManagerId("GRLMRK96M23B018G");
		  for (Iterator iterator = mg.iterator(); iterator.hasNext();)
	    	{
			  OfficeStaff oS= ((OfficeStaff)iterator.next()); 
			  assertEquals("GRLMRK96M23B018G",oS.getManager().getFc());
	    	}
	}
}
