package com.organization.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.organization.entities.Company;
import com.organization.operations.CompanyCRUD;

//Sorts by method name
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCompany {
	
//CREATE OPERATIONS
	@Test
	public void addCompany() {
		Company company1=CompanyCRUD.addCompany("98650738491","IBM");
		Company company2=CompanyCRUD.addCompany("95628975432","Samsung");
		//check if values on DB by using read operation 
		Company ck1=CompanyCRUD.searchCompanyByPiva(company1.getVATnumber());
		assertEquals(company1.toString(),ck1.toString());
		assertNotNull(company1);
		//check if values on DB by using read operation 
		Company ck2=CompanyCRUD.searchCompanyByPiva(company2.getVATnumber());
		assertEquals(company2.toString(),ck2.toString());
		assertNotNull(company2);
	}

//READ OPERATIONS
	@Test
	public void readCompanyes() {
		Set<Company> company= CompanyCRUD.readCompanyes();
		assertNotNull(company);
	}


//UPDATE OPERATIONS
	@Test
	public void updateCompany(){
		Company company=CompanyCRUD.updateCompany("95628975432","Motorola");
		assertEquals("Motorola",company.getNome());
	}


//DELETE OPERATIONS
	@Test
	public void zdeleteCompany(){
		Company company=CompanyCRUD.deleteCompany("98650738491");
		//check if company deleted is the requested
		assertEquals("98650738491",company.getVATnumber());
	}

//SEARCH OPERATIONS	
	@Test
	public void searchCompanyByPiva(){
		Company company=CompanyCRUD.searchCompanyByPiva("95628975432");
		assertEquals("95628975432",company.getVATnumber());

	}
	
	@Test
	public void searchCompanyByName(){
	    Set<Company> companies = new HashSet<>();
	    companies= CompanyCRUD.searchCompanyByName("IBM");
	    //check if Company values returned contains "IBM" in name field
	    for (Iterator iterator = companies.iterator(); iterator.hasNext();)
	    	{
	    	Company company= ((Company)iterator.next()); 
	    	assertEquals("IBM",company.getNome());
	    	}
	}
}
