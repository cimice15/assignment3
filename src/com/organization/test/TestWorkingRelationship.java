package com.organization.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Set;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.organization.entities.Employee;
import com.organization.entities.OfficeStaff;
import com.organization.entities.WorkingRelationship;
import com.organization.operations.EmployeeCRUD;
import com.organization.operations.OfficeStaffCRUD;
import com.organization.operations.WorkingRelationshipCRUD;

//Sorts by method name
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestWorkingRelationship {

	@BeforeClass
    public static void setup() {
		//An office staff and an employee must exists before add a relation between them so:
		Employee emp= EmployeeCRUD.addEmployee("KDPLWO75M82JDOA8","Lucio","Lello");
		assertNotNull(emp);
		OfficeStaff oS= OfficeStaffCRUD.addOfficeStaff("PLFOAK28J33H736X","Pippo","Rossi","impiegato");
		assertNotNull(oS);
		emp= EmployeeCRUD.addEmployee("GRLMSK75H89SJ57A","Giovanni","Storti");
		assertNotNull(emp);
		oS= OfficeStaffCRUD.addOfficeStaff("AJEJEG83J68J937M","Aldo","Baglio","titolare");
		assertNotNull(oS);
		//in this case I have create an extra employee because you could update an istance of working relationship only with an existing employee
		emp= EmployeeCRUD.addEmployee("AAAAADE6M23B019O","Claudio","Rossi");
		assertNotNull(emp);
	}
	
//CREATE OPERATIONS
	@Test
	public void addRapPersEmp() {
	
		WorkingRelationship rl=WorkingRelationshipCRUD.addRapEmpPers("KDPLWO75M82JDOA8","PLFOAK28J33H736X",40096523);
		//check if values on DB by using read operation 
		WorkingRelationship ck1=WorkingRelationshipCRUD.searchWorkByProtN(rl.getNumProt());
		assertEquals(rl.toString(),ck1.toString());
		//also check correctness with test null values
		assertNotNull(rl);
		assertNotNull(ck1);
		WorkingRelationship rl2=WorkingRelationshipCRUD.addRapEmpPers("GRLMSK75H89SJ57A","AJEJEG83J68J937M",96966523);
		//check if values on DB by using read operation 
		WorkingRelationship ck2=WorkingRelationshipCRUD.searchWorkByProtN(rl2.getNumProt());
		assertEquals(rl2.toString(),ck2.toString());
		assertNotNull(rl2);
		assertNotNull(ck2);
		}
		
//READ OPERATIONS
	@Test
	public void readWork() {
		Set<WorkingRelationship> works= WorkingRelationshipCRUD.readWork();
		assertNotNull(works);
	}
	
//DELETE OPERATIONS
	@Test
	public void zdeleteWork() {
		WorkingRelationship work= WorkingRelationshipCRUD.deleteWork(96966523);
		//check eliminated instance
		assertEquals(work.getNumProt(),96966523);
	}
	
//UPDATE OPERATIONS
	
	@Test
	public void updateWorkEmployeeFromId() {
		WorkingRelationship work= WorkingRelationshipCRUD.updateWorkEmployeeFromId(40096523,"AAAAADE6M23B019O");
		assertEquals("AAAAADE6M23B019O",work.getEmp().getFc());
	}
//SEARCH OPERATIONS
	
	@Test
	public void searchWorkByProtNumber() {
		WorkingRelationship work= WorkingRelationshipCRUD.searchWorkByProtN(40096523);
		assertEquals(40096523,work.getNumProt());
		
	}
}
