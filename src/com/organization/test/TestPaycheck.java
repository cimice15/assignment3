package com.organization.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Session;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.organization.entities.Employee;
import com.organization.entities.OfficeStaff;
import com.organization.entities.Paycheck;
import com.organization.entities.Software;
import com.organization.operations.EmployeeCRUD;
import com.organization.operations.OfficeStaffCRUD;
import com.organization.operations.PaycheckCRUD;
import com.organization.operations.SoftwareCRUD;
import com.organization.utility.Hsetup;

//Sorts by method name
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestPaycheck {

	@BeforeClass
    public static void setup() {
		//before add a paycheck
		Employee emp=EmployeeCRUD.addEmployee("OLEKAL84M75P037F","Mirko","Rossi");
		OfficeStaff pu1=OfficeStaffCRUD.addOfficeStaff("ZZZM4QE6M23B018P","Aldo","Fontana","titolare");
		Software sw=SoftwareCRUD.addSoftware("0024UA58HT","DatevCoinos","Coinos","013685648");
		
		Employee emp2=EmployeeCRUD.addEmployee("LFPRKS85M72B027D","Mirko","Storti");
		OfficeStaff pu2=OfficeStaffCRUD.addOfficeStaff("AAAM4QE6M23B018P","Carlo","Pippo","impiegato");
		Software sw2=SoftwareCRUD.addSoftware("624UE99AA","PagheWeb","Zucchetti","0169854126");
	}
	
//CREATE OPERATIONS
	@Test
	public void addPaycheck() {
		Paycheck rl=PaycheckCRUD.addPaycheck("016269854500","gennaio","OLEKAL84M75P037F","ZZZM4QE6M23B018P","0024UA58HT");
		//check if exist
		assertNotNull(rl);
		Session session = Hsetup.getSessionFactory().openSession();
		session.beginTransaction();
		List pCheck = session.createQuery("FROM Paycheck WHERE valNum='016269854500'").list(); 
		assertNotNull(pCheck);
		for (Iterator iterator = pCheck.iterator(); iterator.hasNext();)
	    	{
	    		Paycheck pC= (Paycheck) iterator.next(); 
				assertEquals(pC.getNumVid(),"016269854500");
				assertEquals(pC.getPerComp(),"gennaio");
				assertEquals(pC.getEmp().getFc(),"OLEKAL84M75P037F");
				assertEquals(pC.getOp().getFc(),"ZZZM4QE6M23B018P");
				assertEquals(pC.getSw().getId(),"0024UA58HT");
				assertNotNull(pC);

	    	}
	    session.getTransaction().commit();
	    session.close();

	    //add other paycheck
		Paycheck pC=PaycheckCRUD.addPaycheck("051474ADFW","luglio","LFPRKS85M72B027D","AAAM4QE6M23B018P","624UE99AA");
		//check if exist
		assertNotNull(pC);
		session = Hsetup.getSessionFactory().openSession();
		session.beginTransaction();
		pCheck = session.createQuery("FROM Paycheck WHERE valNum='051474ADFW'").list(); 
		assertNotNull(pCheck);
		for (Iterator iterator = pCheck.iterator(); iterator.hasNext();)
	    	{
	    		pC= (Paycheck) iterator.next(); 
				assertEquals(pC.getNumVid(),"051474ADFW");
				assertEquals(pC.getPerComp(),"luglio");
				assertEquals(pC.getEmp().getFc(),"LFPRKS85M72B027D");
				assertEquals(pC.getOp().getFc(),"AAAM4QE6M23B018P");
				assertEquals(pC.getSw().getId(),"624UE99AA");
				assertNotNull(pC);

	    	}
	    session.getTransaction().commit();
	    session.close();

	}
	
//READ OPERATIONS
	@Test
	public void readPaycheck() {
		Set<Paycheck> pCheck= PaycheckCRUD.readPaycheck();
		assertNotNull(pCheck);
	}

//UPDATE OPERATIONS
	@Test
	public void updatePeriodCompetence(){
		Paycheck emp= PaycheckCRUD.updatePeriodCompetence("016269854500","marzo");
		assertEquals("marzo",emp.getPerComp());
	}


//DELETE OPERATIONS
	@Test
	public void zdeletePaycheck(){
		Paycheck pck=PaycheckCRUD.deletePaycheck("051474ADFW");
		assertEquals("051474ADFW",pck.getNumVid());
	}		
	
//SEARCH OPERATIONS
	@Test
	public void searchPaycheckBySw(){
		Set<Paycheck> pC=PaycheckCRUD.searchPayCheckBySw("624UE99AA");
		//check the id of all software
		for (Iterator iterator = pC.iterator(); iterator.hasNext();)
	    	{
				Paycheck pCheck= (Paycheck) iterator.next(); 
		    	assertEquals("624UE99AA",pCheck.getSw().getId());
	    	}

	}
}
