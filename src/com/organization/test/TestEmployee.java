package com.organization.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.organization.entities.Employee;
import com.organization.operations.EmployeeCRUD;

//Sorts by method name
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestEmployee {

//CREATE OPERATIONS
	@Test
	public void addEmp() {
		Employee emp=EmployeeCRUD.addEmployee("GRLMRK96M23B014G","Mirko","Agarla");
		Employee emp2=EmployeeCRUD.addEmployee("PROWLF94F62K835S","Pietro","Franco");
		//check if values on DB by using read operation 
		Employee ck1=EmployeeCRUD.searchEmployeeByFC(emp.getFc());
		Employee ck2=EmployeeCRUD.searchEmployeeByFC(emp2.getFc());
		assertEquals(emp.toString(),ck1.toString());
		assertNotNull(emp);
		assertEquals(emp2.toString(),ck2.toString());
		assertNotNull(emp2);

	}
		
//READ OPERATIONS
	@Test
	public void readEmployees() {
		Set<Employee> employeeList= EmployeeCRUD.readEmployees();
		assertNotNull(employeeList);
	}

//UPDATE OPERATIONS
	@Test
	public void updateEmployee(){
		Employee emp= EmployeeCRUD.updateEmployee("GRLMRK96M23B014G","Luigi");
		assertEquals("Luigi",emp.getNome());
	}


//DELETE OPERATIONS
	@Test
	public void zdeleteEmployee(){
		Employee emp=EmployeeCRUD.deleteEmployee("PROWLF94F62K835S");
		//check eliminated employee
		assertEquals("PROWLF94F62K835S",emp.getFc());
	}		
	
//SEARCH OPERATIONS
	@Test
	public void searchEmployeeByFC(){
		Employee emp=EmployeeCRUD.searchEmployeeByFC("GRLMRK96M23B014G");
		assertEquals("GRLMRK96M23B014G",emp.getFc());

	}
	
	@Test
	public void searchEmployeeByName(){
	    Set<Employee> employees = new HashSet<>();
	    employees= EmployeeCRUD.searchEmployeeByName("Mirko");
	    //check if Company values returned contains "IBM" in name field
	    for (Iterator iterator = employees.iterator(); iterator.hasNext();)
	    	{
	    	Employee emp= ((Employee)iterator.next()); 
	    	assertEquals("Mirko",emp.getNome());
	    	}
	}

}
