package com.organization.test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Set;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.organization.entities.Company;
import com.organization.entities.Employee;
import com.organization.operations.CompanyCRUD;
import com.organization.operations.WorkCRUD;

//Sorts by method name
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestWork {
	
	@BeforeClass
    public static void setup() {
		Company company= CompanyCRUD.addCompany("08650733491","IBM");
		assertNotNull(company);
	}
	
//CREATE OPERATIONS
	@Test
	public void addWorkEmpCompany(){
		Employee emp= WorkCRUD.addWorkCompanyAndEmp("30050738491","NameCompany1","PLPLPL96M23B019O","Mirko","Agarla");
		Employee emp2= WorkCRUD.addWorkCompanyAndEmp("99950738491","NameCompany2","GRLKAO83J78V637G","Mario","Rossi");
		assertNotNull(emp);
		assertNotNull(emp2);
	}

//UPDATE OPERATIONS
	@Test
	public void updateWorkFromEmp() {
		Employee emp= WorkCRUD.updateWorkFromEmp("PLPLPL96M23B019O","08650733491");
		assertNotNull(emp);
	}
	
//DELETE OPERATIONS
	@Test
	public void zdeleteWork() {
		WorkCRUD.deleteWork("99950738491","GRLKAO83J78V637G");
	}

//SEARCH/READ OPERATIONS
	//return all employees of a company
	@Test
	public void searchWorkFromId(){
		Set<Employee> emp= WorkCRUD.searchWorkFromCompanyId("30050738491");
		assertNotNull(emp);
	}

}
