package com.organization.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Set;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.organization.entities.Software;
import com.organization.operations.SoftwareCRUD;

//Sorts by method name
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestSoftware {
	
//CREATE OPERATIONS
	@Test
	public void addSoftware() {
		Software sw=SoftwareCRUD.addSoftware("1524ua58ht","Helyo","Magnet","013685648");
		//check if values on DB by using read operation 
		Software ck1=SoftwareCRUD.searchSoftwareById(sw.getId());
		assertEquals(sw.toString(),ck1.toString());
		assertNotNull(sw);
		//add other sw
		Software sw2=SoftwareCRUD.addSoftware("6jg8ua58ht","NewSw","NewSoftHouse","548965214");
		Software ck2=SoftwareCRUD.searchSoftwareById(sw2.getId());
		assertEquals(sw2.toString(),ck2.toString());
		assertNotNull(sw2);
	}

//READ OPERATIONS
	@Test
	public void readSw() {
		Set<Software> sw= SoftwareCRUD.readSoftware();
		assertNotNull(sw);
	}

//UPDATE OPERATIONS
	@Test
	public void updateSw() {
		Software sw= SoftwareCRUD.updateSoftwareNameAndHouse("1524ua58ht", "new name", "new software house");
		assertEquals("new name",sw.getSoftN());
		assertEquals("new software house",sw.getSoftH());
	}	

//DELETE OPERATIONS
	@Test
	public void zdeleteSw() {
		Software sw= SoftwareCRUD.deleteSoftwareById("6jg8ua58ht");
		//check if correct object was removed
		assertEquals("6jg8ua58ht",sw.getId());
	}

//SEARCH OPERATION
	@Test
	public void searchSwById() {
		Software sw=SoftwareCRUD.searchSoftwareById("1524ua58ht");
		//check if values on DB by using read operation 
		Software ck1=SoftwareCRUD.searchSoftwareById(sw.getId());
		assertEquals("1524ua58ht",ck1.getId());
	}
}
