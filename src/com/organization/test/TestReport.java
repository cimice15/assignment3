package com.organization.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.organization.entities.OfficeStaff;
import com.organization.entities.Report;
import com.organization.operations.OfficeStaffCRUD;
import com.organization.operations.ReportCRUD;

//Sorts by method name
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestReport {


	@BeforeClass
    public static void setup() {
		OfficeStaff os1= OfficeStaffCRUD.addOfficeStaff("QQQQ4QE6M23B018P","Luca","Bronx","coordinatore");
		OfficeStaff os2= OfficeStaffCRUD.addOfficeStaff("F48d0dE6M23B018P","Paolo","Agarla","titolare");
		assertNotNull(os1);
		assertNotNull(os2);
	}
	

//CREATE OPERATIONS
	@Test
	public void addReport() throws ParseException {
	
		Report rp=ReportCRUD.addReport("l50xwLap","1998-12-23","QQQQ4QE6M23B018P");
		Report rp2=ReportCRUD.addReport("djeuw8az","2015-12-23","RF48d0dE6M23B018P");
		//check if values on DB by using read operation 
		Report ck1=ReportCRUD.searchReportById(rp.getId());
		assertEquals(rp.toString(),ck1.toString());
		assertNotNull(rp);
		assertNotNull(rp2);
		assertNotNull(ck1);

	}
	
//READ OPERATIONS
	@Test
	public void readReport()
	{
		Set<Report> rp= ReportCRUD.readReport();
		assertNotNull(rp);
	}
	
//DELETE OPERATIONS
	@Test
	public void zdeleteReport()
	{
		Report rp= ReportCRUD.deleteReportById("djeuw8az");
		//check if correct object was removed
		assertEquals("djeuw8az",rp.getId());
	}
	
//UPDATE OPERATIONS
	@Test
	public void updateReportDate() throws ParseException {
		Report rp= ReportCRUD.updateReportDate("l50xwLap","2013-10-17");
		SimpleDateFormat formatData = new SimpleDateFormat("yyyy-MM-dd");
		Date dt= rp.getOpblishDate();
		assertEquals("2013-10-17",formatData.format(dt));
	}
	
//SEARCH OPERATIONS
	@Test
	public void searchReportById(){
		Report rp= ReportCRUD.searchReportById("l50xwLap");
		//check if values on DB by using read operation 
		assertEquals("l50xwLap",rp.getId());
	}
}
