package com.organization.operations;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Session;

import com.organization.entities.OfficeStaff;
import com.organization.entities.Report;
import com.organization.utility.DateUtil;
import com.organization.utility.Hsetup;

public class ReportCRUD {

//CREATE OPERATIONS
	public static Report addReport(String id, String pd, String puFc) throws ParseException
	{
		Session session = Hsetup.getSessionFactory().openSession();
		OfficeStaff officeStaff= (OfficeStaff)session.get(OfficeStaff.class, puFc);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date md = format.parse(pd);
		md = DateUtil.addDays(md, 1);
		Report rep = new Report(id,md,officeStaff);
		// start a transaction
		session.beginTransaction();
		session.persist(rep);
		// commit transaction
		session.getTransaction().commit();
		session.close();
		//only way to avoid problems with data during tests. Line 
		//39 must be deleted after tests
	    rep=searchReportById(id);
	    return rep;
	}
	
//READ OPERATIONS
	
	public static Set<Report> readReport()
	{
		Set<Report> rp= new HashSet<>();
		Session session = Hsetup.getSessionFactory().openSession();
		session.beginTransaction();
		List rep = session.createQuery("FROM Report").list(); 
	    for (Iterator iterator = rep.iterator(); iterator.hasNext();)
	    	rp.add((Report) iterator.next()); 			
		session.getTransaction().commit();
	    session.close();
	    return rp;
	}
	
//UPDATE OPERATIONS
	public static Report updateReportDate(String idReport, String data ) throws ParseException{
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date md = format.parse(data);
		md = DateUtil.addDays(md, 1);
		Session session = Hsetup.getSessionFactory().openSession();
		session.beginTransaction();
		Report rep = (Report)session.get(Report.class, idReport); 
		rep.setOpblishDate(md);
		session.update(rep);
		session.getTransaction().commit();
	    session.close();
	    rep=searchReportById(idReport);
	    return rep;
	}
	
//DELETE OPERATIONS
	public static Report deleteReportById(String id)
	{
		Session session = Hsetup.getSessionFactory().openSession();
		session.beginTransaction();
		Report rp= (Report)session.get(Report.class, id);
		session.delete(rp);
		session.getTransaction().commit();
	    session.close();
	    return rp;
	}
//SEARCH OPERATIONS
	public static Report searchReportById(String id)
	{
		Session session = Hsetup.getSessionFactory().openSession();
		session.beginTransaction();
		Report rp= (Report)session.get(Report.class, id);
		session.getTransaction().commit();
	    session.close();
	    return rp;
	}
}
