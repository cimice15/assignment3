package com.organization.operations;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Session;

import com.organization.entities.Software;
import com.organization.utility.Hsetup;

public class SoftwareCRUD {

//ADD OPERATIONS
	public static Software addSoftware(String id, String name, String softH, String telN)
	{
		Session session = Hsetup.getSessionFactory().openSession();
		Software sw = new Software(id,name,softH,telN);
		session.beginTransaction();
		session.persist(sw);
		// commit transaction
		session.getTransaction().commit();
	    session.close();
	    return sw;
	}
	
//READ OPERATIONS
	public static Set<Software> readSoftware()
	{
		Session session = Hsetup.getSessionFactory().openSession();
		Set<Software> softwareList = new HashSet<>();
		// start a transaction
		session.beginTransaction();
		List sw = session.createQuery("FROM Software").list(); 
	    for (Iterator iterator = sw.iterator(); iterator.hasNext();)
	    	softwareList.add((Software) iterator.next()); 			
		// commit transaction
		session.getTransaction().commit();
	    session.close();
	    return softwareList;
	}	
	
//UPDATE OPERATIONS
	//this method updates name and software house of an existing software
	public static Software updateSoftwareNameAndHouse(String id, String name, String softHouse)
	{
		Session session = Hsetup.getSessionFactory().openSession();
		// start a transaction
		session.beginTransaction();
		Software sw= (Software)session.get(Software.class, id);
		sw.setSoftN(name);
		sw.setSoftH(softHouse);
		session.update(sw);
		// commit transaction
		session.getTransaction().commit();
		session.close();
	    return sw;
	}
//DELETE OPERATION
	public static Software deleteSoftwareById(String id)
	{
		Session session = Hsetup.getSessionFactory().openSession();
		// start a transaction
		session.beginTransaction();
		Software sw= (Software)session.get(Software.class, id);
		session.delete(sw);
		// commit transaction
		session.getTransaction().commit();
	    session.close();
	    return sw;
	}	
	
//SEARCH OPERATIONS
	public static Software searchSoftwareById(String id)
	{
		Session session = Hsetup.getSessionFactory().openSession();
		// start a transaction
		session.beginTransaction();
		Software sw= (Software)session.get(Software.class, id);
		// commit transaction
		session.getTransaction().commit();
	    session.close();
	    return sw;
	}
}
