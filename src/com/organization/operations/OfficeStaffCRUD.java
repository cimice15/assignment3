package com.organization.operations;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Session;

import com.organization.entities.OfficeStaff;
import com.organization.utility.Hsetup;

public class OfficeStaffCRUD {

//CREATE OPERATIONS
	public static OfficeStaff addOfficeStaff(String fc, String name, String surname, String role){
			
			Session session = Hsetup.getSessionFactory().openSession();
			OfficeStaff pers = new OfficeStaff(fc,name,surname, role,null,null);
			// start a transaction
			session.beginTransaction();
			session.save(pers);
			// commit transaction
			session.getTransaction().commit();
		    session.close();
		    return pers;
	}
	//this method adds a manager to an existing employee
	public static OfficeStaff addOfficeStaffManagerId(String fc, String name, String surname, String role, String mid){
		
			Session session = Hsetup.getSessionFactory().openSession();
			OfficeStaff manager = (OfficeStaff)session.get(OfficeStaff.class, mid); 
			OfficeStaff pers = (OfficeStaff)session.get(OfficeStaff.class, fc); 
			// start a transaction
			session.beginTransaction();
			pers.setManager(manager);
			session.update(pers); 
			// commit transaction
			session.getTransaction().commit();
		    session.close();
		    return pers;
	}


//READ OPERATIONS

	public static Set<OfficeStaff> readOfficeStaff()
	{
		Session session = Hsetup.getSessionFactory().openSession();
		// start a transaction
		session.beginTransaction();
		List os = session.createQuery("FROM OfficeStaff").list(); 
		Set<OfficeStaff>officeStaffList= new HashSet<>();
	    for (Iterator iterator = os.iterator(); iterator.hasNext();)
	    	officeStaffList.add((OfficeStaff) iterator.next()); 
	    session.getTransaction().commit();
	    session.close();
		
	    return officeStaffList;
	}
	
//UPDATE OPERATIONS
	//this method updates a role of an existing office staff
	public static OfficeStaff updateOfficeStaff(String FC, String role) {
		// create session factory
		Session session = Hsetup.getSessionFactory().openSession();
		session.beginTransaction();
		OfficeStaff oStaff = (OfficeStaff)session.get(OfficeStaff.class, FC); 
		oStaff.setRuolo(role);
		session.update(oStaff); 
		session.getTransaction().commit();
		session.close();
		return oStaff;
	}
			
//DELETE OPERATIONS
	//this method deletes an office staff by fiscal code
	public static OfficeStaff zdeleteOfficeStaffByFc(String FC) {
		Set officeStaffList= searchOfficeStaffByManagerId(FC);
		for (Iterator iterator = officeStaffList.iterator(); iterator.hasNext();)
	       	{
				Session session = Hsetup.getSessionFactory().openSession();
				session.beginTransaction();
				OfficeStaff oS= ((OfficeStaff) iterator.next());
				oS.setManager(null);
				session.update(oS);
				session.getTransaction().commit();
				session.close();

	       	}
		Session session = Hsetup.getSessionFactory().openSession();
		session.beginTransaction();
		OfficeStaff oStaff = (OfficeStaff)session.get(OfficeStaff.class, FC); 
		session.delete(oStaff); 
		session.getTransaction().commit();
		session.close();
		return oStaff;
	}
		
//SEARCH OPERATIONS
	//this method search an office staff by his fiscal code
	public static OfficeStaff searchOfficeStaffByFC(String fc)
	{
		Session session = Hsetup.getSessionFactory().openSession();
		OfficeStaff officeStaff= (OfficeStaff)session.get(OfficeStaff.class, fc);
		// start a transaction
		session.beginTransaction();
		session.persist(officeStaff);
		// commit transaction
		session.getTransaction().commit();
	    session.close();
	    return officeStaff;
	}
	//this method search an office staff by manager id
	public static Set<OfficeStaff> searchOfficeStaffByManagerId(String manId)
	{
		Session session = Hsetup.getSessionFactory().openSession();
		Set<OfficeStaff> officeStaffList= new HashSet<>();
		session.beginTransaction();
		List oS = session.createQuery("FROM OfficeStaff" ).list(); 
	    for (Iterator iterator = oS.iterator(); iterator.hasNext();)
	    	{
	    		OfficeStaff appo=((OfficeStaff) iterator.next());
	    		if(!(appo.getManager()==null))
	    			if(appo.getManager().getFc().equals(manId))
	    				officeStaffList.add(appo);

	    	}
	    session.getTransaction().commit();
	    session.close();
	    return officeStaffList;
	}		
}
