package com.organization.operations;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.organization.entities.Employee;
import com.organization.utility.Hsetup;

public class EmployeeCRUD {

//CREATE OPERATIONS
	public static Employee addEmployee(String fc, String name, String surname){
		
		Session session = Hsetup.getSessionFactory().openSession();
		Employee emp = new Employee(fc,name,surname,null);
		// start a transaction
		session.beginTransaction();
		session.save(emp);
		// commit transaction
		session.getTransaction().commit();
	    session.close();
	    return emp;
}
	
//READ OPERATIONS
	public static Set<Employee> readEmployees()
	{
		Session session = Hsetup.getSessionFactory().openSession();
		session.beginTransaction();
		List employees = session.createQuery("FROM Employee").list(); 
		Set<Employee> employeeList= new HashSet<>();
	    for (Iterator iterator = employees.iterator(); iterator.hasNext();)
           employeeList.add((Employee) iterator.next()); 
        session.getTransaction().commit();
	    session.close();
	    return employeeList;
	}
	
	
//UPDATE OPERATIONS
	//this method updates the name of an existing employee
	public static Employee updateEmployee(String FC, String name) {
		// create session factory
		Session session = Hsetup.getSessionFactory().openSession();
		session.beginTransaction();
		Employee emp = (Employee)session.get(Employee.class, FC); 
		emp.setNome(name);
		session.update(emp); 
		session.getTransaction().commit();
		session.close();
		return emp;
	}
		
//DELETE OPERATIONS
		
	public static Employee deleteEmployee(String FC) {
		// create session factory
		Session session = Hsetup.getSessionFactory().openSession();
		session.beginTransaction();
		Employee emp = (Employee)session.get(Employee.class, FC); 
		session.delete(emp); 
		session.getTransaction().commit();
		session.close();
		return emp;
	}
//SEARCH OPERATIONS
	public static Employee searchEmployeeByFC(String FC)
	{
		Session session = Hsetup.getSessionFactory().openSession();
		Set<Employee> employeeList= new HashSet<>();
		session.beginTransaction();
		Employee company= (Employee)session.get(Employee.class, FC);
	    session.getTransaction().commit();
	    session.close();
	    return company;
	}
	
	public static Set<Employee> searchEmployeeByName(String name)
	{
		Session session = Hsetup.getSessionFactory().openSession();
		Set<Employee> employeeList= new HashSet<>();
		session.beginTransaction();
		List employees = session.createQuery("FROM Employee WHERE Name='"+name+"'" ).list(); 
	    for (Iterator iterator = employees.iterator(); iterator.hasNext();)
	    	employeeList.add((Employee) iterator.next()); 
	    session.getTransaction().commit();
	    session.close();
	    return employeeList;
	}		
}
