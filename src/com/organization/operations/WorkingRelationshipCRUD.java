package com.organization.operations;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Session;

import com.organization.entities.Employee;
import com.organization.entities.OfficeStaff;
import com.organization.entities.WorkingRelationship;
import com.organization.utility.Hsetup;

public class WorkingRelationshipCRUD {

//CREATE OPERATIONS
	public static WorkingRelationship addRapEmpPers(String fcEmp, String fcOp, int protNumber) {
		Session session = Hsetup.getSessionFactory().openSession();
		Employee emp = (Employee)session.get(Employee.class, fcEmp); 
		OfficeStaff officeStaff = (OfficeStaff)session.get(OfficeStaff.class, fcOp); 
		WorkingRelationship rl= new WorkingRelationship(emp,officeStaff,protNumber);
		session.beginTransaction();
		session.persist(rl);
		// commit transaction
		session.getTransaction().commit();
	    session.close();
	    return rl;
	}
	
//READ OPERATIONS 
	public static Set<WorkingRelationship> readWork() {
		Session session = Hsetup.getSessionFactory().openSession();
		session.beginTransaction();
		Set<WorkingRelationship> listWorkRelationship= new HashSet<>();
		List workRel = session.createQuery("FROM WorkingRelationship").list(); 
	    for (Iterator iterator = workRel.iterator(); iterator.hasNext();)
	    	listWorkRelationship.add((WorkingRelationship) iterator.next()); 	
		// commit transaction
		session.getTransaction().commit();
	    session.close();
	    return listWorkRelationship;
	}
	
//DELETE OPERATIONS
	public static WorkingRelationship deleteWork(int protN) {
		Session session = Hsetup.getSessionFactory().openSession();
		session.beginTransaction();
		WorkingRelationship workRel = (WorkingRelationship)session.get(WorkingRelationship.class, protN); 
		session.delete(workRel);
		// commit transaction
		session.getTransaction().commit();
	    session.close();
	    return workRel;
	}
	
//UPDATE OPERATIONS
	//this method updates a work of an existing employee
	public static WorkingRelationship updateWorkEmployeeFromId(int protN,String empCf) {
		Session session = Hsetup.getSessionFactory().openSession();
		session.beginTransaction();
		WorkingRelationship workRel = (WorkingRelationship)session.get(WorkingRelationship.class, protN); 
		Employee emp= (Employee)session.get(Employee.class, empCf);
		workRel.setEmp(emp);
		session.update(workRel);
		// commit transaction
		session.getTransaction().commit();
	    session.close();
	    return workRel;
	}

//SEARCH OPERATIONS 
	//this method returns a work given his protocol number
	public static WorkingRelationship searchWorkByProtN(int protN) {
		Session session = Hsetup.getSessionFactory().openSession();
		session.beginTransaction();
		WorkingRelationship work = (WorkingRelationship)session.get(WorkingRelationship.class, protN); 
		// commit transaction
		session.getTransaction().commit();
	    session.close();
	    return work;
	}
}
