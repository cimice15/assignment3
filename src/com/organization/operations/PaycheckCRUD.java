package com.organization.operations;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Session;

import com.organization.entities.Employee;
import com.organization.entities.OfficeStaff;
import com.organization.entities.Paycheck;
import com.organization.entities.Software;
import com.organization.utility.Hsetup;

public class PaycheckCRUD {

//CREATE OPERATIONS
	public static Paycheck addPaycheck(String valNum, String perComp, String fcEmp, String fcOp, String swId) {

		Session session = Hsetup.getSessionFactory().openSession();
		Employee emp = (Employee)session.get(Employee.class, fcEmp); 
		OfficeStaff officeStaff = (OfficeStaff)session.get(OfficeStaff.class, fcOp); 
		Software sw = (Software)session.get(Software.class, swId); 
		Paycheck payCheck= new Paycheck(valNum,perComp,emp,officeStaff,sw);
		session.beginTransaction();
		session.persist(payCheck);
		// commit transaction
		session.getTransaction().commit();
	    session.close();
	    return payCheck;
	}
	
//READ OPERATIONS
	public static Set<Paycheck> readPaycheck(){
		Set<Paycheck> pC= new HashSet<>(); 
		Session session = Hsetup.getSessionFactory().openSession();
		session.beginTransaction();
		List pCheck = session.createQuery("FROM Paycheck").list(); 
	    for (Iterator iterator = pCheck.iterator(); iterator.hasNext();)
	    	pC.add((Paycheck) iterator.next()); 
	    session.getTransaction().commit();
	    session.close();
	    return pC;
	}
	
//UPDATE OPERATIONS
	//this method updates a period of competence of a paycheck
	public static Paycheck updatePeriodCompetence(String vidN, String perComp ){
		Session session = Hsetup.getSessionFactory().openSession();
		session.beginTransaction();
		Paycheck pCheck = (Paycheck)session.get(Paycheck.class, vidN); 
		pCheck.setPerComp(perComp);
		session.update(pCheck);
		session.getTransaction().commit();
	    session.close();
	    return pCheck;
	}

//DELETE OPERATIONS
	public static Paycheck deletePaycheck(String vidN){
		Session session = Hsetup.getSessionFactory().openSession();
		session.beginTransaction();
		Paycheck pCheck = (Paycheck)session.get(Paycheck.class, vidN); 
		session.delete(pCheck);
		session.getTransaction().commit();
	    session.close();
	    return pCheck;
	}
	
//SEARCH OPERATIONS
	public static Set<Paycheck> searchPayCheckBySw(String swId){
		Set<Paycheck> pC= new HashSet<>(); 
		Session session = Hsetup.getSessionFactory().openSession();
		session.beginTransaction();
		List oS = session.createQuery("FROM Paycheck WHERE 'id'='"+swId+"'").list(); 
	    for (Iterator iterator = oS.iterator(); iterator.hasNext();)
	    	pC.add((Paycheck) iterator.next()); 
	    session.getTransaction().commit();
	    session.close();
	    return pC;
	}
}
