package com.organization.operations;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Session;

import com.organization.entities.Company;
import com.organization.entities.Employee;
import com.organization.utility.Hsetup;

public class WorkCRUD {

//CREATE OPERATIONS
	public static Employee addWorkCompanyAndEmp(String piva, String nameA, String fc, String nameEmp, String cognameEmp) {

		Session session = Hsetup.getSessionFactory().openSession();
		session.beginTransaction();
		Company company1= new Company(piva,nameA);
		Set<Company> company = new HashSet<Company>();
		company.add(company1);
		Employee emp = new Employee(fc,nameEmp,cognameEmp,company);
		session.persist(emp);
		// commit transaction
		session.getTransaction().commit();
	    session.close();
	    return emp;
	}
	
//UPDATE OPERATIONS
	//this method updates an existing company of an existing employee
	public static Employee updateWorkFromEmp(String fc,String piva) {
		// create session factory
		Session session = Hsetup.getSessionFactory().openSession();
		session.beginTransaction();
		Employee emp = (Employee)session.get(Employee.class, fc); 
		Company company= (Company)session.get(Company.class, piva);
		Set<Company> companies = new HashSet<>();
		companies.add(company);
		emp.setCompany(companies);
		session.update(emp); 
		session.getTransaction().commit();
		session.close();
		return emp;
	}

//DELETE OPERATIONS
	public static void deleteWork(String piva, String fc) {
		// create session factory
		Session session = Hsetup.getSessionFactory().openSession();
		session.beginTransaction();
		Company company = (Company)session.get(Company.class, piva); 
		Employee emp = (Employee)session.get(Employee.class, fc); 
		company.setEmp(null);
		emp.setCompany(null);
		session.update(company);
		session.update(emp);
		session.getTransaction().commit();
		session.close();
	}
	
//SEARCH/READ OPERATIONS
	//this method returns all employees of a company
	public static Set<Employee> searchWorkFromCompanyId(String compId)
	{
		Session session = Hsetup.getSessionFactory().openSession();
		Set<Employee> employeeList = new HashSet<>();
		//temp list
		Set<Company> comp;
		// start a transaction
		session.beginTransaction();
		List Allemployees = session.createQuery("FROM Employee").list(); 
		for (Iterator iterator = Allemployees.iterator(); iterator.hasNext();)
			{
				Employee e=((Employee) iterator.next());
				comp=e.getCompany();
				for (Iterator iterator2 = comp.iterator(); iterator2.hasNext();)
				{
					Company cm= ((Company) iterator2.next());
					if(cm.getVATnumber().equals(compId))
						employeeList.add(e);
				}
			}

		// commit transaction
		session.getTransaction().commit();
	    session.close();
	    return employeeList;
	}	
}
