package com.organization.operations;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.organization.entities.Company;
import com.organization.utility.Hsetup;

public class CompanyCRUD {

//ADD OPERATIONS
	public static Company addCompany(String vatNumber, String name)
	{
		Session session = Hsetup.getSessionFactory().openSession();
		Company company = new Company(vatNumber,name);
		// start a transaction
		session.beginTransaction();
		session.persist(company);
		// commit transaction
		session.getTransaction().commit();
	    session.close();
	    return company;
	}
	
	
//READ OPERATIONS
	
	public static Set<Company> readCompanyes()
	{
		Session session = Hsetup.getSessionFactory().openSession();
		session.beginTransaction();
		List companies = session.createQuery("FROM Company").list(); 
		Set<Company> companiesList= new HashSet<>();
        for (Iterator iterator = companies.iterator(); iterator.hasNext();)
           companiesList.add((Company) iterator.next()); 
        session.getTransaction().commit();
	    session.close();
	    return companiesList;
	}
	
	
//UPDATE OPERATIONS
	//this method updates the name of an existing company 
	public static Company updateCompany(String piva, String name) {
		// create session factory
		Session session = Hsetup.getSessionFactory().openSession();
		session.beginTransaction();
		Company company = (Company)session.get(Company.class, piva); 
        company.setNome(name);
		session.update(company); 
		session.getTransaction().commit();
		session.close();
		return company;
	}
	
//DELETE OPERATIONS
	public static Company deleteCompany(String piva) {
		Company company= null;
		// create session factory
		Session session = Hsetup.getSessionFactory().openSession();
		session.beginTransaction();
		company = (Company)session.get(Company.class, piva); 
		session.delete(company); 
		session.getTransaction().commit();
		session.close();
		return company;
	}
	
//SEARCH OPERATION
	public static Company searchCompanyByPiva(String piva)
	{
		Session session = Hsetup.getSessionFactory().openSession();
		session.beginTransaction();
		Company company= (Company)session.get(Company.class, piva);
		session.getTransaction().commit();
	    session.close();
	    return company;
	}
	
	public static Set<Company> searchCompanyByName(String name)
	{
		Session session = Hsetup.getSessionFactory().openSession();
	    Set<Company> companiesList = new HashSet<>();
		session.beginTransaction();
		List companies = session.createQuery("FROM Company WHERE Name='"+name+"'" ).list(); 
		for (Iterator iterator = companies.iterator(); iterator.hasNext();)
		    companiesList.add((Company) iterator.next());
	    session.getTransaction().commit();
	    session.close();
	    return companiesList;
	}

	
}
