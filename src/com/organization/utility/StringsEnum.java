package com.organization.utility;

public enum StringsEnum {
    STR1("coordinatore"),
    STR2("impiegato"),
    STR3("titolare")
    ;

    private final String text;

    /**
     * @param text
     */
    StringsEnum(String text) {
        this.text = text;
    }

    public String getText() {
		return text;
	}
    
    public static StringsEnum inputString(String text) {
        for (StringsEnum b : StringsEnum.values()) {
          if (b.text.equalsIgnoreCase(text)) {
            return b;
          }
        }
        return null;
      }

}